package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 9:51
 * Дифференцирующее идеальное звено
 */
public class BoxDifferIdeal implements ABox {
    private float parK, bufU;

    public BoxDifferIdeal() {
        parK = 0;
        bufU = 0;
    }

    @Override
    public void setParam(int param, float val){
        switch (param){
            case K : parK = val;
                break;
        }
    }

    @Override
    public float get(float input) {
        float res = parK * (input - bufU);
        bufU = input;
        return res;
    }
}
