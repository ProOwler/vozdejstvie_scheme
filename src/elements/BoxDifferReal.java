package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 9:51
 * Дифференцирующее реальное звено
 */
public class BoxDifferReal implements ABox {
    private float parK, parT, bufU, bufY;

    public BoxDifferReal() {
        parK = 0;
        parT = 0;
        bufU = 0;
        bufY = 0;
    }
    @Override
    public void setParam(int param, float val){
        switch (param){
            case K : parK = val;
                break;
            case T1 : parT = val;
                break;
        }
    }

    @Override
    public float get(float input) {
        float res = (parT/(parT+1))*bufY + (parK/(parT+1))*(input - bufU);
        bufU = input;
        bufY = res;
        return res;
    }
}
