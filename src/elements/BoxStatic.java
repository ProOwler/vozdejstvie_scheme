package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 8:49
 * Статическое (безынерционное) звено
 */
public class BoxStatic implements ABox {
    private float parK = 0;

    public BoxStatic() {
    }

    public void setParam(int param, float val){
        if (param == K) parK = val;
    }

    public float get(float input) {
        return input * parK;
    }
}
