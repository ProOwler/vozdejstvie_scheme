package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 9:10
 * Инерционное звено первого порядка (астатическое)
 */
public class BoxInert1 implements ABox {
    private float parK, parT, buf;

    public BoxInert1() {
        parK = 0;
        parT = 0;
        buf = 0;
    }

    public void setParam(int param, float val){
        switch (param){
            case K : parK = val;
                break;
            case T1 : parT = val;
                break;
        }
    }

    @Override
    public float get(float input) {
        // discrete step = 1
        float res = buf*(parT/(parT+1)) + input*(parK/(parT+1));
        buf = res;
        return res;

    }
}
