package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 13:09
 */
public class Element {
    public static final int zStatic = 1;
    public static final int zIntegr = 2;
    public static final int zDiffI =  3;
    public static final int zDiffR =  4;
    public static final int zInert1 = 5;
    public static final int zInert2 = 6;
    public static final int zDelay =  7;
    public static final int zSum =    8;
    public static final int zNegat =  9;

    public static final int K =  11;
    public static final int T1 = 12;
    public static final int T2 = 13;

    private boolean cashed = false;
    private float cashe = 0;
    private ABox zveno;

    private int elemType;
    public int getElemType(){
        return elemType;
    }

    public Element(int type, int delayTicks){
        elemType = type;
        switch (type){
            case zStatic: zveno = new BoxStatic();
                break;
            case zIntegr: zveno = new BoxIntegral();
                break;
            case zDiffI:  zveno = new BoxDifferIdeal();
                break;
            case zDiffR:  zveno = new BoxDifferReal();
                break;
            case zInert1: zveno = new BoxInert1();
                break;
            case zInert2: zveno = new BoxInert2();
                break;
            case zDelay:  zveno = new BoxDelay(delayTicks);
                break;
            case zSum:    zveno = new BoxSummator();
                break;
            case zNegat:  zveno = new BoxNegator();
                break;
        }
    }

    public void setParam(int param, float val){
        switch (param){
            case K:  zveno.setParam(ABox.K, val);
                break;
            case T1: zveno.setParam(ABox.T1, val);
                break;
            case T2: zveno.setParam(ABox.T2, val);
                break;
        }
    }

    public float output(float input){
        if (!cashed) {
            cashe = zveno.get(input);
            cashed = true;
        }
        return cashe;
    }

    public float outputSum(float input1, float input2){
        if (!cashed) {
            cashe = ((BoxSummator)zveno).get(input1, input2);
            cashed = true;
        }
        return cashe;
    }

    public void resetCashe(){
        cashed = false;
    }
}
