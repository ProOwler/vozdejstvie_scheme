package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 11:19
 */
public class BoxInert2 implements ABox {
    private float parK, parT1, parT2, bufY1, bufY2;

    public BoxInert2() {
        parK = 0;
        parT1 = 0;
        parT2 = 0;
        bufY1 = 0;
        bufY2 = 0;
    }

    public void setParam(int param, float val){
        switch (param){
            case K : parK = val;
                break;
            case T1 : parT1 = val;
                break;
            case T2 : parT2 = val;
                break;
        }
    }

    @Override
    public float get(float input) {
        float z = parT2*parT2 + parT1 + 1;
        float res = ((2*parT2*parT2 + parT1)/z)*bufY1 - ((parT2*parT2)/z)*bufY2 + (parK/z)*input;
        bufY2 = bufY1;
        bufY1 = res;
        return res;
    }
}
