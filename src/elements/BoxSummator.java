package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 8:43
 * Returns sum of two inputs

 */
public class BoxSummator implements ABox {

    public BoxSummator() {}

    @Deprecated
    public void setParam(int param, float val) {}

    @Deprecated
    public float get(float input) {
        return 0;
    }

    public float get(float input1, float input2) {
        return input1 + input2;
    }
}
