package elements;

/**
 * User: Prowler
 * Date: 10.01.13
 * Time: 21:35
 * Basic behavior of Scheme Element
 */
public interface ABox {
    public static final int K = 1, T1 = 2, T2 = 3;
    //public void setConversionFunction(Command conversion);
    public void setParam(int param, float val);
    public float get(float input);
}
