package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 8:46
 * Returns negated value
 */
public class BoxNegator implements ABox {
    public BoxNegator() {}

    @Deprecated
    public void setParam(int param, float val) {}

    public float get(float input) {
        return -input;
    }
}
