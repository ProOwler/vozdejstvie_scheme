package elements;

import utils.Buffer;

/**
 * User: Prowler
 * Date: 10.01.13
 * Time: 22:22
 */
public class BoxDelay implements ABox {
    private Buffer buf;

    public BoxDelay(int delayTicks) {
        buf = new Buffer();
        for(int i = 0; i < delayTicks; i++)
            buf.push(0);
    }

    @Deprecated
    public void setParam(int param, float val) {}

    @Override
    public float get(float input){
        buf.push(input);
        return buf.pop();
    }
}
