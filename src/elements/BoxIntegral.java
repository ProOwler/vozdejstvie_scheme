package elements;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 20:49
 * Интегральное звено
 */
public class BoxIntegral implements ABox{
    private float parK, bufU, bufY;

    public BoxIntegral() {
        parK = 0;
        bufU = 0;
        bufY = 0;
    }

    @Override
    public void setParam(int param, float val){
        switch (param){
            case K : parK = val;
                break;
        }
    }

    @Override
    public float get(float input) {
        float res = bufY + (parK/2) * (input + bufU);
        bufU = input;
        bufY = res;
        return res;
    }
}
