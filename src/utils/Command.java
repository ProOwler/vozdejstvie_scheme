package utils;

/**
 * User: Prowler
 * Date: 20.02.12
 * Time: 11:22
 */
public interface Command {
    public float eval(float input);
}
