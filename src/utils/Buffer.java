package utils;

import java.util.ArrayList;

/**
 * User: Prowler
 * Date: 10.01.13
 * Time: 22:07
 * Implementation of QUEUE
 */
public class Buffer {
    private ArrayList<Float> buffer;

    public Buffer() {
        buffer = new ArrayList<Float>();
    }

    public void push(float val){
        buffer.add(val);
    }

    public float pop(){
        if (buffer.isEmpty()){
            return 0;
        }else{
            return buffer.remove(0);
        }
    }
}
