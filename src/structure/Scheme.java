package structure;

import elements.Element;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: Prowler
 * Date: 11.01.13
 * Time: 14:15
 * Класс, определяющий порядок и состав схемы системы управления
 */
public class Scheme {
    private ArrayList<Element> elems;

    private HashMap<Integer, Integer> connections;

    public Scheme() {
        connections = new HashMap<Integer, Integer>();
        elems = new ArrayList<Element>();
    }

    /*
    * todo: реализовать список элементов и связь между ними
    * Вариант:
    * - дин.массив для элементов (всех)
    * - дин.массив для связей -
    *   индекс элемента массива = индексу элемента схемы,
    *   элемент массива = индексу элемента, являющегося входом данного,
    *    для сумматоров значение = -1
    * - карта дин.массивов для сумматоров -
    *   ключ = индекс сумматора,
    *   элементы массива - входы сумматора
    * */
    public void setInput(int elementIdx, int inputIdx){
        //elems.a
    }

    public int createElement(int type, int delayTicks){
        elems.add(new Element(type, delayTicks));
        return elems.size()-1;
    }

 }
